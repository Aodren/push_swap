/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 14:17:44 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 22:15:24 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

void		ft_smallest_elem(t_pile **pilea)
{
	int min;
	int index;

	index = 0;
	if (*pilea)
	{
		min = ft_lst_min(*pilea, &index);
		if (index == 0)
		{
			while ((*pilea)->nbr != min)
			{
				ft_putstr("ra ");
				ft_ra_rb(*pilea);
			}
		}
		else
		{
			while ((*pilea)->nbr != min)
			{
				ft_putstr("rra ");
				ft_rra_rrb(*pilea);
			}
		}
	}
}

static void	ft_push_elemb(t_pile **pilea, t_pile **pileb)
{
	ft_smallest_elem(pilea);
	ft_putstr("pb ");
	ft_pa_pb(pileb, pilea);
}

void		ft_algo_push_swap(t_pile **pilea, t_pile **pileb)
{
	int i;

	i = 0;
	ft_tri_simple(pilea);
	while (!ft_lst_is_tri(*pilea))
		ft_push_elemb(pilea, pileb);
	if (*pileb)
		i = ft_lst_strlen(*pileb);
	while (i > 0)
	{
		ft_pa_pb(pilea, pileb);
		if (i > 1)
			ft_putstr("pa ");
		else
			ft_putendl("pa");
		--i;
	}
}
