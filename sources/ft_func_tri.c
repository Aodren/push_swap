/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_func_tri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 21:07:20 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 16:20:40 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdlib.h>
#include "libft.h"

/*
** sa : swap a - intervertit les 2 premiers éléments au sommet de la pile a.
** (ne fait rien s’il n’y en a qu’un ou aucun).
** sb : swap b idem
*/

void	ft_sa_sb(t_pile *pila)
{
	if (pila && pila->next)
		ft_lst_swap_pile(pila, pila->next);
}

/*
**	sa + sb
*/

void	ft_ss(t_pile *pila, t_pile *pilb)
{
	ft_sa_sb(pila);
	ft_sa_sb(pilb);
}

/*
** pa : push a - prend le premier élément au sommet de b et le met sur a.
** (ne fait rien si b est vide).
** pb : push b : idem inverser pila avec pilb lors de l'appel
*/

void	ft_pa_pb(t_pile **pila, t_pile **pilb)
{
	t_pile	*begin;
	t_pile	*lst;
	int		nbr;

	nbr = (*pilb)->nbr;
	lst = *pila;
	if (*pilb)
	{
		lst = *ft_add_elem_pill_sommet(pila, nbr);
		begin = *pilb;
		*pilb = (*pilb)->next;
		if (begin)
			free(begin);
		begin = NULL;
	}
	*pila = lst;
}

void	ft_ra_rb(t_pile *pila)
{
	while (pila->next)
	{
		ft_lst_swap_pile(pila, pila->next);
		pila = pila->next;
	}
}

/*
**  rr : ra et rb en meme temps.
*/

void	ft_rr(t_pile *pila, t_pile *pilb)
{
	ft_ra_rb(pila);
	ft_ra_rb(pilb);
}
