/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_pill_a.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 19:12:46 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 20:30:52 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

t_pile	*ft_fill_pill_a(int argc, char **argv)
{
	t_pile	*begin;
	int		i;
	long	nbr;

	i = 1;
	begin = NULL;
	while (i < argc)
	{
		nbr = ft_atol(*(argv + i));
		if (nbr > 2147483647 || nbr < -2147483648
				|| ft_check_doublon(begin, nbr))
		{
			if (begin)
				ft_clear_lst(begin);
			return (NULL);
		}
		begin = *ft_add_elem_pill(&begin, nbr);
		++i;
	}
	return (begin);
}
