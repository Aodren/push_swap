/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_swap.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 21:05:27 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 16:22:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

/*
** correction : ft_display_pile(pila);
*/

t_pile			*ft_push_swap(t_pile *pila)
{
	t_pile *pilb;

	pilb = NULL;
	ft_algo_push_swap(&pila, &pilb);
	return (pila);
}
