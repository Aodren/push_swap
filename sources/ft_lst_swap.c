/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_swap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 14:39:17 by abary             #+#    #+#             */
/*   Updated: 2016/02/21 16:37:44 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_lst_swap_pile(t_pile *elema, t_pile *elemb)
{
	int nbr;

	if (elema && elemb)
	{
		nbr = elema->nbr;
		elema->nbr = elemb->nbr;
		elemb->nbr = nbr;
	}
}
