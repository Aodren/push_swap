/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_elem_pill.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 19:48:28 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 21:32:36 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"
#include <stdlib.h>

void	ft_display_pile(t_pile *begin)
{
	t_pile	*pile;

	pile = begin;
	while (pile)
	{
		ft_putnbr(pile->nbr);
		if (pile->next)
			ft_putchar(' ');
		else
			ft_putchar('\n');
		pile = pile->next;
	}
}

void	ft_clear_lst(t_pile *begin)
{
	if (begin->next)
		ft_clear_lst(begin->next);
	begin->next = NULL;
	if (begin)
		free(begin);
	begin = NULL;
}

t_pile	**ft_add_elem_pill(t_pile **begin, int nbr)
{
	t_pile	*pile;
	t_pile	*lst;

	pile = NULL;
	pile = (t_pile *)ft_memalloc(sizeof(t_pile));
	pile->nbr = nbr;
	pile->next = NULL;
	lst = *begin;
	if (*begin)
	{
		while (lst->next)
			lst = lst->next;
		lst->next = pile;
	}
	else
		begin = &pile;
	return (begin);
}

t_pile	**ft_add_elem_pill_sommet(t_pile **begin, int nbr)
{
	t_pile	*pile;

	pile = (t_pile *)ft_memalloc(sizeof(t_pile));
	pile->nbr = nbr;
	pile->next = NULL;
	if (*begin)
	{
		pile->next = *begin;
		(*begin) = pile;
	}
	else
		begin = &pile;
	return (begin);
}
