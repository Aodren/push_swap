/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pile_inverse_last.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 18:04:25 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 16:21:14 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

void	ft_pile_inverse_last(t_pile **a)
{
	int		min;
	t_pile	*pil;

	pil = *a;
	while (pil->next)
	{
		min = pil->nbr;
		pil = pil->next;
	}
	if (min > pil->nbr)
	{
		ft_rra_rrb(*a);
		ft_putstr("rra ");
		ft_rra_rrb(*a);
		ft_putstr("rra ");
		ft_sa_sb(*a);
		ft_putstr("sa ");
		ft_ra_rb(*a);
		ft_putstr("ra ");
		ft_ra_rb(*a);
		ft_putstr("ra ");
	}
}
