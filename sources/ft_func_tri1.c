/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_func_tri1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 17:13:49 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 17:47:26 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

/*
** rra : reverse rotate a
** (vers le bas, le dernier élément devient le premier).
*/

void	ft_rra_rrb(t_pile *lista)
{
	if (lista->next)
		ft_rra_rrb(lista->next);
	ft_lst_swap_pile(lista, lista->next);
}

/*
**  rrr : rra et rrb en même temps.
*/

void	ft_rrr(t_pile *pilea, t_pile *pileb)
{
	ft_rra_rrb(pilea);
	ft_rra_rrb(pileb);
}
