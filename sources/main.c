/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 21:29:04 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 15:52:53 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "push_swap.h"
#include <stdlib.h>

int			main(int argc, char **argv)
{
	t_pile *pila;

	pila = NULL;
	if (argc == 1)
		return (0);
	if (ft_check_args(argc, argv))
	{
		if ((pila = ft_fill_pill_a(argc, argv)))
			pila = ft_push_swap(pila);
		else
			ft_putendl("Error");
	}
	else
		ft_putendl("Error");
	if (pila)
		ft_clear_lst(pila);
	return (0);
}
