/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tri_simple.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 15:08:18 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 18:16:09 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

static	void	ft_tri_small_lst(t_pile **pilea)
{
	if (!ft_lst_is_tri(*pilea) &&
			(*pilea)->nbr > (*pilea)->next->nbr)
	{
		ft_sa_sb(*pilea);
		if (ft_lst_is_tri(*pilea))
			ft_putendl("sa");
		else
			ft_putstr("sa ");
	}
}

static void		ft_tri_smallest(int min, t_pile **pilea)
{
	while ((*pilea)->nbr != min)
	{
		ft_rra_rrb(*pilea);
		if (ft_lst_is_tri(*pilea))
			ft_putendl("rra");
		else
			ft_putstr("rra ");
	}
}

static void		ft_tri_smallest_end(t_pile **pilea)
{
	int		min;
	int		index;
	int		ok;
	t_pile	*a;

	min = ft_lst_min(*pilea, &index);
	a = *pilea;
	ok = 1;
	while (a)
	{
		if (a->nbr == min)
		{
			while (a && ok)
			{
				index = a->nbr;
				a = a->next;
				if (a && index > a->nbr)
					ok = 0;
			}
			break ;
		}
		a = a->next;
	}
	if (ok)
		ft_tri_smallest(min, pilea);
}

static	void	ft_tri_3_elem(t_pile **pilea)
{
	ft_putstr("rra ");
	ft_rra_rrb(*pilea);
	ft_putendl("sa");
	ft_sa_sb(*pilea);
}

void			ft_tri_simple(t_pile **pilea)
{
	int len;

	len = ft_lst_strlen(*pilea);
	ft_tri_small_lst(pilea);
	ft_tri_smallest_end(pilea);
	if (len == 3 && !ft_lst_is_tri(*pilea))
		ft_tri_3_elem(pilea);
	if (!ft_lst_is_tri(*pilea))
		ft_pile_inverse_last(pilea);
}
