/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 16:08:26 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 16:21:46 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_lst_strlen(t_pile *pile)
{
	int nbr;

	nbr = 0;
	if (pile == 0)
		return (0);
	while (pile)
	{
		++nbr;
		pile = pile->next;
	}
	return (nbr);
}

/*
**	Retourne le nombre du dernier element de la liste
*/

int		ft_lst_last_nbr(t_pile *pile)
{
	if (!pile)
		return (0);
	while (pile->next)
		pile = pile->next;
	return (pile->nbr);
}

/*
**	retourne 1 si fonction trie sinon retourne 0
*/

int		ft_lst_is_tri(t_pile *pile)
{
	while (pile->next)
	{
		if (pile->next->nbr > pile->nbr)
			pile = pile->next;
		else
			return (0);
	}
	return (1);
}

/*
**	recreer une liste a partie de la 2 postion
*/

t_pile	**ft_recreate_pile(t_pile **pile)
{
	t_pile *tmp;
	t_pile *new;

	new = 0;
	tmp = *pile;
	if (tmp)
		tmp = tmp->next;
	while (tmp)
	{
		new = *ft_add_elem_pill(&new, tmp->nbr);
		tmp = tmp->next;
	}
	ft_clear_lst(*pile);
	*pile = 0;
	*pile = new;
	return (pile);
}

/*
** index 0 : premiere moitie
** index 1 : deuxieme moitie
*/

int		ft_lst_min(t_pile *pile, int *index)
{
	int min;
	int pos;
	int nbr;
	int	len;

	len = ft_lst_strlen(pile);
	pos = 0;
	nbr = 0;
	min = pile->nbr;
	while (pile)
	{
		if (pile->nbr < min)
		{
			pos = nbr;
			min = pile->nbr;
		}
		pile = pile->next;
		++nbr;
	}
	if (pos == 0 || len / pos < 2)
		*index = 1;
	else
		*index = 0;
	return (min);
}
