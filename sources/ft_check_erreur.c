/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_erreur.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 21:32:47 by abary             #+#    #+#             */
/*   Updated: 2016/02/24 20:38:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "push_swap.h"

static int	ft_check_one_arg(char *str)
{
	if (*str == '-' || *str == '+')
		++str;
	while (*str)
	{
		if (!ft_isdigit(*str))
			return (0);
		++str;
	}
	return (1);
}

int			ft_check_args(int argc, char **argv)
{
	int i;

	if (argc == 1)
		return (0);
	i = 1;
	while (i < argc)
	{
		if (!(ft_check_one_arg(*(argv + i))))
			return (0);
		++i;
	}
	return (1);
}

int			ft_check_doublon(t_pile *begin, int nbr)
{
	t_pile *pile;

	pile = begin;
	while (pile)
	{
		if (pile->nbr == nbr)
			return (1);
		pile = pile->next;
	}
	return (0);
}
