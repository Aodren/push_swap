# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/02/25 20:49:09 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = push_swap.a\

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)
#CFLAGS = -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC =  main.c ft_check_erreur.c ft_fill_pill_a.c ft_add_elem_pill.c\
	   ft_push_swap.c ft_func_tri.c ft_lst_swap.c ft_func_tri1.c\
	   ft_algo.c ft_lst_tools.c ft_tri_simple.c ft_pile_inverse_last.c

SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)


$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
#	clang-3.5 -o $(NAME) $(LIB_DIR)/libft.a $(NAME_LIB) libft/libft.a push_swap.a
#	gcc -o $(NAME) $(LIB_DIR)/libft.a $(NAME_LIB) libft/libft.a push_swap.a -fsanitize=address -g
	gcc -o $(NAME) $(LIB_DIR)/libft.a $(NAME_LIB) push_swap.a

all : $(NAME)

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean flcean re
