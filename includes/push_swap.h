/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 21:42:52 by abary             #+#    #+#             */
/*   Updated: 2016/02/25 15:46:12 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

/*
*******************************************************************************
**								LISTE										  *
*******************************************************************************
*/
typedef struct	s_pile
{
	int				nbr;
	struct s_pile	*next;

}				t_pile;
t_pile			*ft_fill_pill_a(int argc, char **argv);
t_pile			**ft_add_elem_pill(t_pile **begin, int nbr);
t_pile			**ft_add_elem_pill_sommet(t_pile **begin, int nbr);
void			ft_clear_lst(t_pile *begin);
void			ft_display_pile(t_pile *begin);
void			ft_lst_swap_pile(t_pile *elema, t_pile *elemb);
t_pile			**ft_recreate_pile(t_pile **pile);
int				ft_lst_strlen(t_pile *pile);
int				ft_lst_last_nbr(t_pile *pile);
int				ft_lst_is_tri(t_pile *pile);
int				ft_lst_min(t_pile *pile, int *index);
/*
*******************************************************************************
**							ERRORS MANAGEMENT								  *
*******************************************************************************
*/

int				ft_check_args(int argc, char **argv);
int				ft_check_doublon(t_pile *begin, int nbr);

/*
*******************************************************************************
**								FUNCTIONS TRI								  *
*******************************************************************************
*/

t_pile			*ft_push_swap(t_pile *pila);
void			ft_sa_sb(t_pile *pila);
void			ft_ss(t_pile *pila, t_pile *pilb);
void			ft_pa_pb(t_pile **pila, t_pile **pilb);
void			ft_ra_rb(t_pile *pila);
void			ft_rra_rrb(t_pile *pila);
void			ft_rr(t_pile *pila, t_pile *pileb);
void			ft_rrr(t_pile *pilea, t_pile *pileb);
void			ft_algo_push_swap(t_pile **pila, t_pile **pileb);
void			ft_tri_simple(t_pile **pilea);
void			ft_pile_inverse_last(t_pile **pilea);
#endif
